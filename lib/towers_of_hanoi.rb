# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    #   tower:     0       1   2
    @towers = [[3, 2, 1], [], []] # [base,mid,top]
  end

  def self.play
    print "Lets start!\n"
    game = TowersOfHanoi.new

    until game.won?
      print game.render + "\n\n"
      print "#{game.towers}\n"

      print "What tower do you want to move a disk from?\n"
      from_tower = gets.chomp
      break if game.quit_command?(from_tower)

      print "Now what tower do you want put that disk in?\n"
      to_tower = gets.chomp

      break if game.quit_command?(to_tower)

      to_tower = to_tower.to_i - 1
      from_tower = from_tower.to_i - 1

      if game.valid_move?(from_tower, to_tower)
        game.move(from_tower, to_tower)

      else print "Thats an invalid move, try again\n"
      end

    end

    if game.won?
      print game.render + "\n\n"
      #print "#{game.towers}\n"
      print "Great job, you're good at this!\n"
      print "One more rounds ?\n"
      print "Y/N\n"
      prompt = gets.chomp
      restart if prompt.upcase == "Y"
    end

    print "Have a great day!\n"

  end

  def render
    #[[3, 2, 1], [], []]
    tower_img = @towers.map(&:reverse)
    # p"-----reverse-----"
    # p "tower_img"
    # p tower_img
    # p"-----reverse-----"
    # print "\n\n\n"
    # #[[1, 2 ,3], [], []]

    tower_img.map do |nest|
    	if !nest.empty?
        2.times { nest.unshift 0 } if nest.length == 1
        nest.unshift 0 if nest.length == 2
      else 3.times { nest.unshift 0 }
    	end
    end

    # p"-----fill-----"
    # p "tower_img"
    # p tower_img
    # p"-----fill-----"
    # print "\n\n\n"

    tower_img = tower_img.map do |nest|
    		nest.map do |el|
    			case el
          when 1 then   "   -|-   "
          when 2 then   "  --|--  "
          when 3 then   " ---|--- "
          when 0 then 	"    |    "
          end
         end
     end

    # p"-----draw-----"
    # p "tower_img"
    # p tower_img
    # p"-----draw-----"
    # print "\n\n\n"


    tower_img = tower_img.transpose.map(&:join).join("\n")


    # p"-----print-----"
    # p "tower_img"
    # p tower_img
    # p"-----print-----"
    # print "\n\n\n"

    tower_img


  end

  def self.restart
    self.play
  end
  #load "towers_of_hanoi.rb"
  #TowersOfHanoi.play

  def quit_command?(string)
    ["stop", "reset", "restart", "exit", "quit"].include?(string.downcase)
  end

  def move(from_tower, to_tower)
    @towers[to_tower].push @towers[from_tower].pop
  end

  def valid_move?(from_tower, to_tower)
    return false if from_tower.class != Fixnum || to_tower.class != Fixnum
    return false if @towers[from_tower].nil? || @towers[from_tower].empty?

    from_tower_disk = @towers[from_tower].last
    to_tower_disk = @towers[to_tower].last

    # return true if from_tower_disk < to_tower_disk
    return false if !to_tower_disk.nil? && from_tower_disk > to_tower_disk
    true

  end

  def won?

    if @towers[1].length == 3
      true
    elsif @towers[2].length == 3
      true
    end

  end

  if __FILE__ == $PROGRAM_NAME
    TowersOfHanoi.play
  end

end
